const { request } = require('express');
const express = require('express');
const converter = require("./converter");
const app = express();
const port = 3000;

app.get('/', (req,res) => res.send("Hello there!"));

app.get('/hex-to-rgb', (req, res) => {
    const red = parseInt(req.query.red, 16);
    const green = parseInt(req.query.green, 16);
    const blue = parseInt(req.query.blue, 16);
    const hex = red.toString() + green.toString() + blue.toString();
    res.send(hex);
});


if(process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => console.log(`Server: localhost:${port}`));
}

