/**
 * Padding outputs 2 characters allways
 * @param {string} hex on or two characters
 * @returns {string} hex with two characters
 */

// const pad = (hex) => {
//  return (hex.length === 1 ? "0" + hex : hex);

   


module.exports = {
    /**
     * Converts RGB to HEX string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex value
     */
    hexToRgb: (red, green, blue) => {
       
        const redHex = parseInt(red, 16); 
        const greenHex = parseInt(green, 16);    
        const blueHex = parseInt(blue, 16); 
        const rgb = `${redHex},${greenHex},${blueHex}`;
        return rgb;
       // const hex = pad(redHex) + pad(greenHex) + pad(blueHex);
       // console.log({hex});
       // return hex; // hex string 6 characters
    }
    
}